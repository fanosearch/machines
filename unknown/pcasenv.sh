#!/bin/bash

###############################################################
# Sets up environment variables for pcas
###############################################################
# Author: Tom Coates
###############################################################

# This is empty, as we are not running on a known cluster
# Please replace this with an environment setup appropriate
# to your situation.  Template:

# export PCAS_LOGD_SERVER=localhost
# export PCAS_LOGD_PORT=12354
# export PCAS_STOREDB_SERVER=localhost
# export PCAS_STOREDB_PORT=12352
# export PCAS_LOCKD_SERVER=localhost
# export PCAS_LOCKD_PORT=12351
# export PCAS_KVDBD_SERVER=localhost
# export PCAS_KVDBD_PORT=12356
# export PCAS_METRICSDBD_SERVER=localhost
# export PCAS_METRICSDBD_PORT=12355
# export PCAS_MONITORD_SERVER=fano.ma.ic.ac.uk
# export PCAS_MONITORD_PORT=12357
# export PCAS_FSD_SERVER=segre.ma.ic.ac.uk
# export PCAS_FSD_PORT=12358
