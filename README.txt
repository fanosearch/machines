README.txt
==========

This repository contains cluster-specific or machine-specific scripts
to allow our Fanosearch job scripts to run on that cluster or machine.
The subdirectory machines/foo is for the foo cluster or machine, and
should contain:

qsub          A script for submitting jobs to the PBS scheduler (if any)
qdel          A script for deleting jobs from the PBS scheduler (if any)
qstat         A script for getting the status of jobs submitted to the PBS
                       scheduler (if any)
create_user   A script to set up a new user on that machine or cluster
whichcluster  A script to output which cluster we are running on (if any)
pcasenv.sh   A script to set environment variables for pcas infrastructure.
